import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';


import { Redirect } from "react-router-dom";
import history from '../history';
import { withRouter } from "react-router-dom";


// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">


//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function SignIn() {
  const classes = useStyles();
 
const [userName,setuserName] = useState('');
const [password,setpassword] = useState('');
const [access_token,setaccess_token] = useState("");


function handleSubmit(e){
    e.preventDefault()
    
    let data = {"username":userName,"password":password}  

    console.log("data",data)

    const formData = new FormData();

    formData.append('username', userName);
    formData.append('password', password);
  
  fetch('http://0.0.0.0:8001/token', {
        method: 'POST',
        body: formData

    })
    .then((response) => response.json())
.then((result) => {
  console.log('Success:', result);
  if (result.access_token){
    localStorage.setItem("access_token", result.access_token);

    setaccess_token("value")  
  }else{

setaccess_token("The username or password you entered is incorrect") 
  }
  
})
.catch((error) => {
  setaccess_token("The username or password you entered is incorrect") 

  console.error('Error:', error);
  
});


   }

    const styleObj = {
    float: "left"
    }

    const styleObj1 = {
    color:"red",
    float: "left"
    }

if (localStorage.getItem("access_token") != null){
  return(
    <Redirect to="/home" />
    )
}

else { 

  return (

    <Container component="main" maxWidth="xs">
      <CssBaseline />
      
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>

        
        <form className={classes.form} Validate onSubmit={handleSubmit}>
          
	     <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="userName"
                label="Username"
                name="userName"
                onChange={e => setuserName(e.target.value)}
                autoComplete="uname"
              />
            </Grid>

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={e => setpassword(e.target.value)}
          />

          <div style={styleObj1}>
            <span>{access_token}
          </span>        
        </div>

        
          <div style={styleObj}> 
          <FormControlLabel
            control={<Checkbox value="remember" color="primary"/>}
            label="Remember me"
          />
          </div>
          <Grid item xs={12}>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit} 
             onChange={(e) => handleSubmit(e)}
         
          >
            Sign In
          </Button>
           </Grid>


  
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="/Signup" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>

        </form>
      </div>
    </Container>
  );
}
}
export default withRouter(SignIn);
