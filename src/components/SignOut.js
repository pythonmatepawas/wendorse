import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Redirect } from "react-router-dom";
import history from '../history';
import { withRouter } from "react-router-dom";



const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function SignOut() {
 
  const classes = useStyles();
 
    const [logout,setlogout] = useState("");
  

  function handleSubmit(e){
    e.preventDefault()
    localStorage.removeItem("access_token")
    setlogout("value")
   }

    const styleObj = {
  
    color: "red",
    float: "left",
    }
if (localStorage.getItem('access_token') == null){
  return(
    <Redirect to="/" />
    )
}else{
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
     
        <Typography component="h1" variant="h5">
          Sign Out
        </Typography>


        <form className={classes.form} Validate onSubmit={handleSubmit}>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onChange={(e) => handleSubmit(e)}
            
          >
            SIGN OUT
          </Button>
           </form>
      </div>
    </Container>

  );
}
}
export default SignOut;