import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Redirect } from "react-router-dom";
import history from '../history';
import { withRouter } from "react-router-dom";



const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function SignUp() {
 
  const classes = useStyles();
 
    const [firstName,setfirstName] = useState('');
    const [lastName,setlastName] = useState('');
    const [userName,setuserName] = useState('');
    const [email,setemail] = useState('');
    const [phone,setphone] = useState('');
    const [password,setpassword] = useState('');
    const [confirmpassword,setconfirmpassword] = useState('');
    const [password_validate,setpassword_validate] = useState('');
    const [email_validate,setemail_validate] = useState('');
    const [login,setlogin] = useState("");
  

  const isUpperCase = (password) => /(?=.*[A-Z])/.test(password)
  const isLowerCase = (password) => /(?=.*[a-z])/.test(password)
  const isDigit = (password) => /(?=.*[0-9])/.test(password)
  const isEmail = (email) => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)
  

function validate_email(e){
  if (!isEmail(e)) {
    setemail_validate('Please Enter a valid email')

  }else{
    setemail(e)
    setemail_validate(null)

  }
}
 function validate_pass(e){
  if (e.length <= 7){
    setpassword_validate('Your password must be at least 8 characters long.')
  } else if (!isUpperCase(e)) {
        setpassword_validate('Have a mixture of uppercase and lowercase letters.')

  }
  else if (!isLowerCase(e)) {
        setpassword_validate('Have a mixture of uppercase and lowercase letters.')

  }
  else if (!isDigit(e)) {
        setpassword_validate('Contain at least one number')

  }
  else {
    setpassword(e)
    setpassword_validate(null)
  }


 }
 function handleSubmit(e){
    e.preventDefault()    
  
    let data = {"firstname":firstName,"lastname":lastName,"username":userName,"email":email,"phone_number":phone,"password":password}
    
    
    fetch('http://0.0.0.0:8001/users/', {
        method: 'POST',
        body: JSON.stringify(data)
    })
    .then((response) => {
       console.log("response",response)
       setlogin("value")
    }).catch(error =>{
        console.log(error)
    });
   }

    const styleObj = {
  
    color: "red",
    float: "left",
    }
if (login == "value"){
  return(
    <Redirect to="/" />
    )
}else{
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>


        <form className={classes.form} Validate onSubmit={handleSubmit}>
        

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                type="text"
                id="firstName"      
                onChange={e => setfirstName(e.target.value)}
                label="First Name"
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                onChange={e => setlastName(e.target.value)}
                autoComplete="lname"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="userName"
                label="Username"
                name="userName"
                onChange={e => setuserName(e.target.value)}
                autoComplete="uname"
              />
            </Grid>
              
              <Grid item xs={12}>
              <TextField
                variant="outlined"
                fullWidth
                type="email"
                id="email"
                label="Email"
                name="email"
                onChange={e => validate_email(e.target.value)}
                autoComplete="email"
               
              />
             <div style={styleObj}> 
              <span>{email_validate}</span></div>
            </Grid>

            <Grid item xs={12}>

              <TextField
                variant="outlined"
                required
                fullWidth
                type="number"
                id="phone"
                max="5"
                label="Mobile Number"
                name="phone"
                value={phone}
                onChange={e => setphone(e.target.value)}
                autoComplete="phone"
                 /> 
                
                 
              </Grid>
              
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={e => validate_pass(e.target.value)}
                autoComplete="current-password"
                

              /> 
              <div style={styleObj}> 
              <span>{password_validate}</span></div>
            </Grid>


            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="confirm_password"
                label="Confirm Password"
                type="password"
                id="password"
                onChange={e => setconfirmpassword(e.target.value)}
                autoComplete="current-password"
              />
            </Grid>
            </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onChange={(e) => handleSubmit(e)}
            
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>

  );
}
}
export default withRouter(SignUp);