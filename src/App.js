import React from 'react';
import './App.css';
import Signin from './components/Signin'
import SignUp from './components/Signup'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import Appbar from './components/Appbar'
import history from './history';
import SignOut from './components/SignOut'
import Home from  './components/Home'



function App() {
  return (
    <div className="App">
    <Appbar/>
    <Router >
    <Route exact path="/" component={Signin} exact history={history} />
    <Route path="/signup" component={SignUp} history={history} exact/>
    <Route path="/signout" component={SignOut} />
    <Route path="/home" component={Home} />
    
  


    </Router>

    </div>
  );
}

export default App;
